CREATE DATABASE QLSV;
USE QLSV;
CREATE TABLE DMKHOA(
    MaKH varchar(6) primary key,
    TenKhoa varchar((30))
);
CREATE TABLE SINHVIEN(
    MaSV varchar(6) primary key,
    HoSV varchar(30),
    TenSV varchar(15),
    GioiTinh char(1),
    NgaySinh DateTime,
    NoiSinh varchar(50),
    DiaChi varchar(50),
    MaKH varchar(6),
    HocBong int
);